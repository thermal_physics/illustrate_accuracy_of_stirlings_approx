"""
Illustrate the accuracy of Stirling's approximation:
  n! \approx n log n -n
for low n
"""

import six
if six.PY2: raise RuntimeError("Use Python 3 or greater")
import os
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as sc
import scipy.special as sf

def main():
    nmax = 10
    ns = np.array(list(range(0, nmax)))
    exacts = np.exp(sf.loggamma(ns + 1))

    cns = np.linspace(0.01, nmax, 1000)
    cexacts = np.exp(sf.loggamma(cns + 1))

    cstirling_1 = np.exp(cns * np.log(cns) - cns)

    cstirling_2 = cstirling_1 * np.sqrt(2 * np.pi * cns)

    cstirling_3 = cstirling_2 * (1.0 + 1.0 / 12.0 / cns)

    plt.ion()
    base_filename = os.path.splitext(__file__)[0]
    with plt.style.context(base_filename + ".mplstyle"):
        fig = plt.figure()
        ax = fig.add_axes([0.2, 0.15, 0.77, 0.80])
        ax.semilogy(ns, exacts, "+", label=r"$n!$")
        ax.semilogy(cns, cexacts, "-", label=r"$\Gamma(n+1)$")
        alabel_1 = r"$n ! \approx e^{n \ln n -n}$"
        ax.semilogy(cns, cstirling_1, "-", label=alabel_1)
        alabel_2 = r"$n ! \approx e^{n \ln n -n} \sqrt{2 \pi n}$"
        ax.semilogy(cns, cstirling_2, "-", label=alabel_2)
        alabel_3 = (r"$n ! \approx e^{n \ln n -n} \sqrt{2 \pi n} " +
                    r"\left(1 + \frac{1}{12n} \right)$")
        ax.semilogy(cns, cstirling_3, "-", label=alabel_3)
        ax.set_xlim([-0.5, nmax])
        ax.legend()
        ax.set_ylim([0.1, 1e7])
        ax.set_xlabel(r"$n$")
        ax.set_ylabel(r"$n!$", rotation=0.0, labelpad=20)
        plt.savefig(base_filename + "_generated.pdf")

if __name__ == "__main__":
    main()










